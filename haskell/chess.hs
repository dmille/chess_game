import Control.Monad

data Piece = Pawn Color | King Color | Queen Color | Bishop Color | Knight Color | Rook Color | Empty deriving (Eq)
data Color = Blk | Wht | None deriving (Eq)
data Dir = N | S | E | W | NW | NE | SE | SW | NNW | NNE | SSW | SSE | NEE | NWW | SEE | SWW deriving (Eq,Show,Read)
data Board = Board [[Piece]] [Piece] [Piece]

main = forever $ do
         putStrLn(show game)
         input <- getLine
         let split = splitBy ' ' input
         let pos = split !! 0
         let dir = getDir (split !! 1)
         let steps = getSteps split
         putStrLn(show(moveN pos dir steps game))
--         putStrLn("Pos: " ++ pos ++ "\nDir: " ++ show dir ++ "\nSteps: " ++ show steps)
--         moveN pos dir steps game

play :: [[Char]] -> Board -> Board
play [] g = g
play (p:ls) g = do
  let split = splitBy ' ' p
      pos = split !! 0
      dir = getDir (split !! 1)
      steps = getSteps split
  play ls (moveN pos dir steps g)
  
--takes string and 
getPos :: [Char] -> [Char]
getPos l = take 2 l

getDir :: [Char] -> Dir
getDir l = read l

getSteps :: [[Char]] -> Intg
getSteps l
    | length l == 3 = read (l !! 2)
    | otherwise = 8

splitBy delimiter = foldr f [[]] 
            where f c l@(x:xs) | c == delimiter = []:l
                             | otherwise = (c:x):xs

--inital state of the board
game = Board [ [Rook Blk, Knight Blk, Bishop Blk, Queen Blk, King Blk, Bishop Blk, Knight Blk, Rook Blk],
               [Pawn Blk, Pawn Blk,   Pawn Blk,   Pawn Blk,  Pawn Blk, Pawn Blk,   Pawn Blk,   Pawn Blk],
               [Empty,    Empty,      Empty,      Empty,     Empty,    Empty,      Empty,      Empty   ],
               [Empty,    Empty,      Empty,      Empty,     Empty,    Empty,      Empty,      Empty   ],
               [Empty,    Empty,      Empty,      Empty,     Empty,    Empty,      Empty,      Empty   ],
               [Empty,    Empty,      Empty,      Empty,     Empty,    Empty,      Empty,      Empty   ],                                                            [Pawn Wht, Pawn Wht,   Pawn Wht,   Pawn Wht,  Pawn Wht, Pawn Wht,   Pawn Wht,   Pawn Wht],
               [Rook Wht, Knight Wht, Bishop Wht, Queen Wht, King Wht, Bishop Wht, Knight Wht, Rook Wht] 
             ] [] []

test = Board [ [Rook Blk, Knight Blk, Bishop Blk, Queen Blk, King Blk, Bishop Blk, Knight Blk, Rook Blk],
               [Pawn Blk, Pawn Blk,   Pawn Blk,   Pawn Blk,  Pawn Blk, Pawn Blk,   Pawn Blk,   Pawn Blk],
               [Empty,    Empty,      Empty,      Empty,     Empty,    Empty,      Empty,      Empty   ],
               [Empty,    Empty,      Empty,      Empty,     Empty,    Empty,      Empty,      Empty   ],
               [Empty,    Empty,      Queen Wht,      Empty,     Empty,    Empty,      Empty,      Empty   ],
               [Empty,    Empty,      Empty,      Empty,     Empty,    Empty,      Empty,      Empty   ],                                             
               [Empty,    Empty,      Empty,      Empty,     Empty,    Empty,      Empty,      Empty   ],                                             
               [Rook Wht, Knight Wht, Bishop Wht, Queen Wht, King Wht, Bishop Wht, Knight Wht, Rook Wht] 
             ] [] []

--Show instance for displaying a Color
instance Show Color where
    show Blk = "B"
    show Wht = "W"

--Show instance for displaying a single Piece
instance Show Piece where
    show (Pawn x) = "P" ++ show x
    show (King x) = "K" ++ show x
    show (Queen x) = "Q" ++ show x
    show (Bishop x) = "B" ++ show x
    show (Knight x) = "N" ++ show x
    show (Rook x) = "R" ++ show x
    show (Empty) = "  "

--Show instance for displaying the board
instance Show Board where
    show (Board [] w b) = "   " ++ take (5*8-1) (repeat '-') ++ "\n    " ++ foldl (\xs c-> c:("    " ++ xs)) "" (reverse [ c | c <- ['a'..'h']]) ++ "\nWhite Pieces: " ++ show w ++ "\nBlack Pieces: " ++ show b
    show (Board (x:xs) w b) = "   " ++ take (5*8-1) (repeat '-') ++ "\n" ++ show (length xs + 1) ++" | " ++ unwords (map (\f -> (show f)++" |") x) ++ "\n" ++  show (Board xs w b)

--checks if direction is allowed for given Piece
isValidDir (Pawn Blk) d
    | d == S = True
    | d == SE = True
    | d == SW = True
    | otherwise = False

isValidDir (Pawn Wht) d
    | d == N = True
    | d == NE = True
    | d == NW = True
    | otherwise = False

isValidDir (King {}) d
    | d == NNW = False
    | d == NNE = False
    | d == SSW = False
    | d == SSE = False
    | otherwise = True

isValidDir (Queen {}) d
    | d == NNW = False
    | d == NNE = False
    | d == SSW = False
    | d == SSE = False    
    | otherwise = True

isValidDir (Bishop {}) d
    | d == NW = True
    | d == NE = True
    | d == SW = True
    | d == SE = True
    | otherwise = False

isValidDir (Knight {}) d
    | d == NNW = True
    | d == NNE = True
    | d == SSW = True
    | d == SSE = True
    | d == SEE = True
    | d == NEE = True
    | d == SWW = True
    | d == NWW = True
    | otherwise = False

isValidDir (Rook {}) d
    | d == N = True
    | d == E = True
    | d == S = True
    | d == W = True
    | otherwise = False

isValidDir (Empty) d = False

--Turns a char to it's appropriate number for tuplefy
digitToInt :: Char -> Int
digitToInt 'a' = 1
digitToInt 'b' = 2
digitToInt 'c' = 3
digitToInt 'd' = 4
digitToInt 'e' = 5
digitToInt 'f' = 6
digitToInt 'g' = 7
digitToInt 'h' = 8
digitToInt '1' = 1
digitToInt '2' = 2
digitToInt '3' = 3
digitToInt '4' = 4
digitToInt '5' = 5
digitToInt '6' = 6
digitToInt '7' = 7
digitToInt '8' = 8
digitToInt _ = (-1)

--Helper function for moveList in case Piece jumps (ie Knight)
fixMoveList :: [(Int,Int)] -> [(Int,Int)]
fixMoveList [(row,col)]
    | row > 8 = []
    | row < 1 = []
    | col > 8 = []
    | row < 1 = []
    | otherwise = [(row,col)]

--Create a list of n spots to be moved through 
moveListN :: (Int,Int) -> Piece -> Dir -> Int -> [(Int,Int)]
moveListN (row,col) p d n = take (n+1) (moveList (row,col) p d)

--Create a list of spots to be moved through
moveList :: (Int,Int) -> Piece -> Dir -> [(Int,Int)]
moveList (row,col) (Pawn c) N = zip [row,row+1] (repeat col)
moveList (row,col) p N = zip [row..8] (repeat col)
moveList (row,col) (Pawn c) S = zip [row,row-1] (repeat col)
moveList (row,col) p S = zip [row,row-1..1] (repeat col)
moveList (row,col) p E = zip (repeat row) [col..8]
moveList (row,col) p W = zip (repeat row) [col,col-1..1]
moveList (row,col) (Pawn c) NE = zip [row,row+1] [col,col+1]
moveList (row,col) p NE = zip [row..8] [col..8]
moveList (row,col) (Pawn c) NW = zip [row,row+1] [col,col-1]
moveList (row,col) p NW = zip [row..8] [col,col-1..1]
moveList (row,col) (Pawn c) SE = zip [row,row-1] [col,col+1]
moveList (row,col) p SE = zip [row,row-1..1] [col..8]
moveList (row,col) (Pawn c) SW = zip [row,row-1] [col,col-1]
moveList (row,col) p SW = zip [row,row-1..1] [col,col-1..1]
moveList (row,col) p NNE = fixMoveList [(row+2,col+1)]
moveList (row,col) p NNW = fixMoveList [(row+2,col-1)]
moveList (row,col) p SSE = fixMoveList [(row-2,col+1)]
moveList (row,col) p SSW = fixMoveList [(row-2,col-1)]
moveList (row,col) p NEE = fixMoveList [(row+1,col+2)]
moveList (row,col) p NWW = fixMoveList [(row+1,col-2)]
moveList (row,col) p SEE = fixMoveList [(row-1,col+2)]
moveList (row,col) p SWW = fixMoveList [(row-1,col-2)]

--Returns spot to go to
targetSpot :: [(Int,Int)] -> Piece -> Board -> (Int,Int)
targetSpot (t:n:xs) (King c) b
    | xs /= [] = targetSpot (t:n:[]) (King c) b
--pawn behaviour
targetSpot ((tx,ty):(nx,ny):xs) (Pawn c) b
    | ty /= ny && (pieceAt (nx,ny) b) == Empty = (tx,ty)
    | ty == ny && (pieceAt (nx,ny) b) /= Empty = (tx,ty)
    | otherwise = (nx,ny)
targetSpot (t:n:xs) p b
    | (pieceAt n b) /= Empty && (colorOf p) == (colorOf (pieceAt n b)) = t --and make sure colors are same
    | (pieceAt n b) /= Empty && (colorOf p) /= (colorOf (pieceAt t b)) = n
    | otherwise = targetSpot (n:xs) p b
targetSpot (t:[]) _ _ = t

--Returns color of Piece
colorOf :: Piece -> Color
colorOf (Pawn x) = x
colorOf (King x) = x
colorOf (Queen x) = x
colorOf (Bishop x) = x
colorOf (Knight x) = x
colorOf (Rook x) = x
colorOf Empty = None

--Retrieve Piece at (row,col) tuple
pieceAt :: (Int,Int) -> Board -> Piece
pieceAt (row,col) (Board ls w b) = (ls !! (8-row)) !! (col-1)

--Turn Char pair input into single (row,col) tuple
tuplefy :: [Char] -> (Int,Int)
tuplefy [col, row] = (digitToInt row, digitToInt col)

--Helper function for replaceAtIndex
replace n item row = a ++ (item:b) where
    (a, (_:b)) = splitAt n row

--Replaces piece at index tuple with new piece and adds the removed piece to out list
replaceAtIndex :: (Int,Int) -> Piece -> Board -> Board
replaceAtIndex (x,y) item (Board ls w b)
--    | pieceAt (x,y) (Board ls w b) == item = (Board ls w b)
    | colorOf (pieceAt (x,y) (Board ls w b)) == Blk = 
        let row = ls !! (8-x)
            mod_row = replace (y-1) item row
        in Board (replace (8-x) mod_row ls) w ((pieceAt (x,y) (Board ls w b)):b)
    | colorOf (pieceAt (x,y) (Board ls w b)) == Wht = 
        let row = ls !! (8-x)
            mod_row = replace (y-1) item row
        in Board (replace (8-x) mod_row ls) ((pieceAt (x,y) (Board ls w b)):w) b
    | otherwise =
        let row = ls !! (7-x+1)
            mod_row = replace (y-1) item row
        in Board (replace (7-x+1) mod_row ls) w b

--remove the Piece that was just moved
popMoved :: (Int,Int) -> Board -> Board
popMoved (x,y) (Board ls w b) =
    let row = ls !! (7-x+1)
        mod_row = replace (y-1) Empty row
    in Board (replace (7-x+1) mod_row ls) w b

--Same as move, except it takes an additional parameter n which is the number of spots to traverse
moveN :: [Char] -> Dir -> Int -> Board -> Board
moveN f d n g
    | isValidDir (pieceAt (tuplefy f) g) d == True = 
        let t = tuplefy f
            p = pieceAt t g
            m = moveListN t p d n
            tar = targetSpot m p g
        in (if tar == t then g else popMoved t (replaceAtIndex tar p g))
    | otherwise = g
