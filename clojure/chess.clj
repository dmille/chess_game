(require '[clojure.string :as str])

(ns a4.core
  (:require [clojure.string :as str]))

(def moves {:N nil :S nil :E nil :W nil :NE nil :NW nil :SE nil :SW nil :NNW nil :NNE nil :SSW nil :SSE nil :SEE nil :NEE nil :SWW nil :NWW nil})

(def validMoves {:r #{:N :S :E :W} :n #{:NNW :NNE :SSW :SSE :SEE :NEE :SWW :NWW} :b #{:NW :NE :SW :SE} :q #{:N :S :E :W :NW :SW :NE :SE} :k #{:N :S :E :W :NW :SW :NE :SE} :p #{:N :S :NE :NW :SE :SW}})

(def colors {:b "B" :w "W"})

(def pieces {:r "R" :n "N" :b "B" :q "Q" :k "K" :p "P" :empty "  "})

(defn corr [target] (if (or 
                         (< (:row target) 0) 
                         (> (:row target) 8) 
                         (< (:col target) 0) 
                         (> (:col target) 8)) nil target))

(def lseq {:S #(map (fn [x] {:row x}) (take (+ % 1) (iterate dec %)))
           :N #(map (fn [x] {:row x}) (take (- 8 %) (iterate inc %)))
           :W #(map (fn [x] {:col x}) (take (+ % 1) (iterate dec %)))
           :E #(map (fn [x] {:col x}) (take (- 8 %) (iterate inc %)))
           :R #(map (fn [x] {:row x}) (take 8 (repeat %)))
           :C #(map (fn [x] {:col x}) (take 8 (repeat %)))
           })

;; (:DIR moves) row col -> [{:row x0 :col y0},{:row x1 :col y1},{:row xn :col yn}]
(def moves {:N (fn [row col] (map conj ((:N lseq) row) ((:C lseq) col)))
            :S (fn [row col] (map conj ((:S lseq) row) ((:C lseq) col)))
            :E (fn [row col] (map conj ((:R lseq) row) ((:E lseq) col)))
            :W (fn [row col] (map conj ((:R lseq) row) ((:W lseq) col)))
            :NE (fn [row col] (map conj ((:N lseq) row) ((:E lseq) col)))
            :NW (fn [row col] (map conj ((:N lseq) row) ((:W lseq) col)))
            :SE (fn [row col] (map conj ((:S lseq) row) ((:E lseq) col)))
            :SW (fn [row col] (map conj ((:S lseq) row) ((:W lseq) col)))
            :NNW (fn [row col] [{:row row :col col} (corr {:row (+ row 2) :col (- col 1)})])
            :NNE (fn [row col] [{:row row :col col} (corr {:row (+ row 2) :col (+ col 1)})])
            :SSW (fn [row col] [{:row row :col col} (corr {:row (- row 2) :col (- col 1)})])
            :SSE (fn [row col] [{:row row :col col} (corr {:row (- row 2) :col (+ col 1)})])
            :SEE (fn [row col] [{:row row :col col} (corr {:row (- row 1) :col (+ col 2)})])
            :NEE (fn [row col] [{:row row :col col} (corr {:row (+ row 1) :col (+ col 2)})])
            :SWW (fn [row col] [{:row row :col col} (corr {:row (- row 1) :col (- col 2)})])
            :NWW (fn [row col] [{:row row :col col} (corr {:row (+ row 1) :col (- col 2)})])
            })

(def game2(atom {:board [[{:p :r :c :b},{:p :n :c :b},{:p :b :c :b},{:p :q :c :b},{:p :k :c :b},{:p :b :c :b},{:p :empty},{:p :empty}],
                   [{:p :p :c :b},{:p :p :c :b},{:p :p :c :b},{:p :p :c :b},{:p :p :c :b},{:p :p :c :b},{:p :empty},{:p :p :c :b}],
                   [{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty}],
                   [{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty}],
                   [{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty}],
                   [{:p :r :c :w},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty}],                 
                   [{:p :empty},{:p :p :c :w},{:p :p :c :w},{:p :p :c :w},{:p :p :c :w},{:p :p :c :w},{:p :empty},{:p :p :c :w}],
                   [{:p :r :c :w},{:p :n :c :w},{:p :b :c :w},{:p :q :c :w},{:p :k :c :w},{:p :b :c :w},{:p :n :c :w},{:p :r :c :w}]
                   ]
           :w []
           :b []
           }))

(def game(atom {:board [[{:p :r :c :b},{:p :n :c :b},{:p :b :c :b},{:p :q :c :b},{:p :k :c :b},{:p :b :c :b},{:p :n :c :b},{:p :r :c :b}],
                   [{:p :p :c :b},{:p :p :c :b},{:p :p :c :b},{:p :p :c :b},{:p :p :c :b},{:p :p :c :b},{:p :p :c :b},{:p :p :c :b}],
                   [{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty}],
                   [{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty}],
                   [{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty}],
                   [{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty},{:p :empty}],                 
                   [{:p :p :c :w},{:p :p :c :w},{:p :p :c :w},{:p :p :c :w},{:p :p :c :w},{:p :p :c :w},{:p :p :c :w},{:p :p :c :w}],
                   [{:p :r :c :w},{:p :n :c :w},{:p :b :c :w},{:p :q :c :w},{:p :k :c :w},{:p :b :c :w},{:p :n :c :w},{:p :r :c :w}]
                   ]
           :w []
           :b []
           }))

;;  return the cell at given coordinates
(defn cellAt
  [tuple]
  (get-in @game [:board (- 7 (:row tuple)) (:col tuple)]))

;; return the piece at given coordinates
(defn getPieceAt
  [tuple]
  (:p (cellAt tuple)))

;; returns true if the spot is empty
(defn isEmpty
  [tuple]
  (= (getPieceAt tuple) :empty))

;; handle those pesky pawn moves
(defn pawnMove
  [moves]
  (let [own (first moves) next (last moves)]
    (if (= (:col own) (:col next))
      (if (isEmpty next) next own) 
      (if (isEmpty next) own next) )
    ))

;; changes contents of a space to another piece
(defn putAt
  [g tuple piece]
  (assoc-in g [:board (- 7 (:row tuple)) (:col tuple)] piece))

;; return the color at a given coordinates
(defn getColor
  [tuple]
  (:c (cellAt tuple)))

;; moves piece to out list
(defn capturePiece
  [g tuple]
  (update-in g [(getColor tuple)] #(into [] (concat % [(cellAt tuple)])) ))

;; removes piece at given location
(defn popPiece
  [g tuple]
  (putAt g tuple {:p :empty}))

;; moves piece from one space to another
(defn movePiece
  [g from to]
  (let [piece (cellAt from)]
    (swap! g popPiece from)
    (if (not(isEmpty to))
      (swap! g capturePiece to))
    (swap! g putAt to piece)
    ))

;; return true if the colors of the pieces are the same
(defn isSameColor
  [tuple1 tuple2]
  (= (getColor tuple1)(getColor tuple2)))

;; returns a vector of hash-mapsn
(defn moveList
  [tuple dir]
  ( (dir moves) (:row tuple) (:col tuple)))

;; returns true if direction for the piece is allowed
(defn isValidDir
  [piece dir]
  (= (get-in validMoves [piece dir]) dir))

;; print the contents of the given cell
(defn strCell
  [cell]
  ( if(number? cell) cell (str (get pieces 
             (get cell :p)) 
        (get colors 
             (get cell :c))
        )))

(defn strLine
  [a]
  (str/join (take 8 (repeat (str "+" (str/join(take 4 (repeat "-"))))))))

;; Converts board to string
(defn toStr
  [g]
  ( map(fn [n] (map #(strCell %) n)) g))

(defn toStrOut
  [s]
  (map #(strCell %) s))

;; adds numbers to each row of the chessboard
(defn addnums
  [g]
  (let [list (take 8(iterate dec 8))]
  (map #(flatten(vector %1 %2)) list (:board g))))

;; prints the board
(defn printBoard
  [g] 
  (let [b (addnums g)]
    (print(str "   " (strLine 1) "+\n " 
               (str/join
                (str/join 
                 (str "\n   " 
                      (strLine 1) "+\n ")) 
                (map #(str (str/join " | " %) " |") 
                     (toStr b))) "\n   " (strLine 1) "+\n     "))
    (print(str/join "    " (map #(str(char %)) (take 8(iterate inc 97)))))
    (print(str "\nWhite: " (str/join ", " (toStrOut(:w g)))))
    (print(str "\nBlack: " (str/join ", " (toStrOut(:b g)))))))

;; Converts char to it's appropriate int
(defn charToNum [ch] (if (> (int ch) 56) (-(int ch) 97) (-(int ch) 49)) )

;; Converts char pair into a tuple for the coordinates
(defn tuplefy [coord] (hash-map :row (charToNum(last coord)), 
                                :col (charToNum(first coord))
                                ))

;; return a tuple of the cell to move to
(defn target
  [moves]
  (let [pair (take 2(filter #(not (isEmpty %)) moves)) empty (last(take-while #(isEmpty %) (rest moves)))] 
    (if (nil? (seq pair))
      (first moves)
      (if (= (getPieceAt(first pair)) :p) 
        (pawnMove (take 2 moves)) 
        (if (isSameColor (first pair) (last pair) ) 
          (if(nil? empty) (first pair) empty) 
          (last pair)))
      )))

;; move.. taking a coord (ex "a3") and a dir (ex :N)  (fnil target (take (+ 1 num) moves))
(defn move
  ([coord dir]
   (move coord dir 8))
  ([coord dir num]
   (let [tuple (tuplefy coord) 
         moves (moveList tuple dir)
         targ (target (take (+ 1 num) moves)) 
         piece (getPieceAt tuple)]
     (if(isValidDir piece dir)
       (printBoard(movePiece game (first moves) targ))
       (printBoard @game)))))

;; wrapper for move. takes a string, parses it and calles move
(defn strInput
  [string]
  (let [args (str/split string #"\s") 
        coord (get args 0) 
        dir (keyword(get args 1)) 
        num (if(not= (peek args) (get args 1)) 
              (Integer. (get args 2)) 
              8 )]
    (if(contains? moves dir)
      (move coord dir num)
      (printBoard @game))))

(defn -main
  "I do everything"
  [& args]
  (printBoard @game)
  (while true 
    (println "")
    (strInput (read-line))
    ))
(-main)
