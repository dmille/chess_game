%% TO RUN THIS CODE:
%% Run erl, compile, run main().
%%
%% $ erl
%% 1> c(a5).
%% 2> a5:main().
%%
%% Coded by David Miller, without assistance
%% 500398647

-module(a5).
-export([p/0]).

%% To be removed after. Just for testing purposes
-compile(export_all).

-record(p, {t,c}).

%% drop n elements from beginning of list
drop(_,[]) ->
    [];
drop(0,List) ->
    List;
drop(N,[_H|T]) ->    
    drop(N-1,T).

%% zips together two lists of equal and UNEQUAL length
zip2([],_) ->
    [];
zip2(_,[]) ->
    [];
zip2([H1|T1],[H2|T2]) ->
    lists:append([{H1,H2}],zip2(T1,T2)).

%% for every n elements of a list, perform Fun over it
forevery(_,_,_,[]) ->
    "";
forevery(N,F,X,List) ->
    io:format(lists:concat([X, " "])),
    F(lists:sublist(List,N)),
    forevery(N,F,X-1,drop(N,List)).

%% Check if Direction is valid
isValidDir({p,q,_}, Dir) ->
    lists:member(Dir,[n,s,w,e,ne,nw,se,sw]);
isValidDir({p,k,_}, Dir) ->
    lists:member(Dir,[n,s,w,e,ne,nw,se,sw]);
isValidDir({p,r,_}, Dir) ->
    lists:member(Dir,[n,s,w,e]);
isValidDir({p,b,_}, Dir) ->
    lists:member(Dir,[ne,nw,se,sw]);
isValidDir({p,n,_}, Dir) ->
    lists:member(Dir,[nnw,nne,ssw,sse,nee,nww,see,sww]);
isValidDir({p,p,b}, Dir) ->
    lists:member(Dir,[s,se,sw]);
isValidDir({p,p,w}, Dir) ->
    lists:member(Dir,[n,ne,nw]);
isValidDir(_,_) ->
    false.

%% Create a list of moves for the piece in the direction given
moves({X,Y},n) ->
    zip2(lists:seq(X,0,-1),lists:duplicate(8,Y));
moves({X,Y},s) ->
    zip2(lists:seq(X,7),lists:duplicate(8,Y));
moves({X,Y},e) ->
    zip2(lists:duplicate(8,X),lists:seq(Y,7));
moves({X,Y},w) ->
    zip2(lists:duplicate(8,X),lists:seq(Y,0,-1));
moves({X,Y},ne) ->
    zip2(lists:seq(X,0,-1),lists:seq(Y,7));
moves({X,Y},nw) ->
    zip2(lists:seq(X,0,-1),lists:seq(Y,0,-1));
moves({X,Y},se) ->
    zip2(lists:seq(X,7),lists:seq(Y,7));
moves({X,Y},sw) ->
    zip2(lists:seq(X,7),lists:seq(Y,0,-1)).

corrK([F,{X,Y}]) ->
    if X > 7 -> [F,F];
       X < 0 -> [F,F];
       Y > 7 -> [F,F];
       Y < 0 -> [F,F];
       true ->[F,{X,Y}]
    end.

% get pair knight moves
knightMoves({X,Y},nne) -> corrK([{X,Y},{X-2,Y+1}]);
knightMoves({X,Y},nnw) -> corrK([{X,Y},{X-2,Y-1}]);
knightMoves({X,Y},nee) -> corrK([{X,Y},{X-1,Y+2}]);
knightMoves({X,Y},nww) -> corrK([{X,Y},{X-1,Y-2}]);
knightMoves({X,Y},sse) -> corrK([{X,Y},{X-2,Y+1}]);
knightMoves({X,Y},ssw) -> corrK([{X,Y},{X-2,Y-1}]);
knightMoves({X,Y},see) -> corrK([{X,Y},{X-1,Y+2}]);
knightMoves({X,Y},sww) -> corrK([{X,Y},{X-1,Y-2}]).

%% if there is only one element in the list duplicate that element
corrM([F|[]]) ->
    [F,F];
corrM([F|L]) ->
    [F|L].

%% Get pair of pawn moves
pawnMoves([{X0,Y0},{X1,Y1}|_],Board) ->
    C0 = getColor({X0,Y0},Board),
    C1 = getColor({X1,Y1},Board),
    P1 = getPiece({X1,Y1},Board),
    if C0 == C1 -> [{X0,Y0},{X0,Y0}];
       true ->
	    if Y0 == Y1 -> 
 		    if P1 == e -> [{X0,Y0},{X1,Y1}];
		       true -> [{X0,Y0},{X0,Y0}]
		    end;
	       true ->
		    if P1 == e -> [{X0,Y0},{X0,Y0}];
		       true -> [{X0,Y0},{X1,Y1}]
		    end    
	       end
       end.

%% create pair of moves 
movesList({X,Y},N,Dir,Board) ->
    P = getPiece({X,Y},Board),
    if P == k -> lists:sublist(corrM(moves({X,Y},Dir)),2);
       P == n -> knightMoves({X,Y},Dir);
       P == p -> pawnMoves(corrM(moves({X,Y},Dir)),Board);
       true -> lists:sublist(corrM(moves({X,Y},Dir)),N+1)
    end.

%% produces pair of spots where piece should move from -> to
targetPair({X,Y},N,Dir, Board) ->
    P = getPiece({X,Y}, Board),
    List = movesList({X,Y},N,Dir,Board),
    Elist = lists:takewhile(fun(E) -> getPiece(E,Board) == e end, lists:nthtail(1,List)),
    if length(Elist) == 0 -> Empty = [];
       true -> Empty = lists:last(Elist)
    end,
    Pair = lists:filter(fun(E) -> getPiece(E,Board) /= e end, List),
    Me = lists:nth(1,Pair),
    if P == n -> Targ = lists:nth(2,List);
       P == p -> Targ = lists:nth(2,List);
       true ->
	    if length(Pair) == 1 -> Targ = Empty;
	       true -> Targ = lists:nth(2,Pair)
	    end
    end,
    MeColor = getColor(Me,Board),
    TargColor = getColor(Targ,Board),
    if Me == Targ -> [Me,Targ];
       MeColor == TargColor -> 
	    if Empty == [] -> [Me,Me];
	       true -> [Me,Empty]
	    end;
       MeColor /= TargColor -> [Me,Targ];
       true -> io:format("missing condition")
    end.

%% Change nth element to New
setnth(1,[_|Tail],New) -> [New|Tail];			  
setnth(N,[H|Tail],New) -> [H|setnth(N-1,Tail,New)].

popPiece([_,Targ], Board, {W,B}) ->
    TargPiece = getCell(Targ, Board),
    Color = getColor(Targ,Board),
    if Color == w -> {lists:append(W,[TargPiece]),B};	    
       Color == b -> {W,lists:append(B,[TargPiece])};
       true -> {W,B}
    end.
    
%% Moves Me to Targ
movePiece([Me,Targ], Board) ->
    MePiece = getCell(Me, Board),
    B2 = setnth(index(Targ),Board,MePiece),
    setnth(index(Me),B2,#p{t=e,c=e}).

%% prints horizontal seperator
line() ->
    "  +"++lists:duplicate(8,lists:flatten(lists:duplicate(4,"-"),"+")).

%% prints piece
printPiece({p,e,_}) ->
    io:format(" ");
printPiece({p,r,_}) ->
    io:format("R");
printPiece({p,b,_}) ->
    io:format("B");
printPiece({p,n,_}) ->
    io:format("N");
printPiece({p,q,_}) ->
    io:format("Q");
printPiece({p,k,_}) ->
    io:format("K");
printPiece({p,p,_}) ->
    io:format("P").

%% prints color
printColor({p,_,b}) ->
    io:format("B");
printColor({p,_,w}) ->
    io:format("W");
printColor(_) ->
    io:format(" ").

%% prints the contents of a row
printRow(Row) ->
    lists:foreach(fun(N) -> 
			  io:format(" "),
			  printPiece(N),
			  printColor(N),
			  io:format(" |")
		  end, Row).

%% prints the contents of Board
printBoard(Board) ->    
    io:format("~n~n"++line()++"~n"),
    forevery(8, fun(R) ->
			io:format("|"),
			printRow(R),
			io:format("~n"++line()++"~n")
		end,8, Board),
    io:format("    "++lists:foldr(fun(X,S) -> 
				  lists:flatten([X])++"    "++S
				end ,"",lists:seq(97,104))).

%% prints offboard characters
printOffBoard({W,B}) ->
    io:format("\nWhite: "),
    lists:foreach(fun(N) -> 
			  printPiece(N),
			  io:format(", ")
		  end, W),
    io:format("\nBlack: "),
    lists:foreach(fun(N) -> 
			  printPiece(N),
			  io:format(", ")
		  end,B),
    io:format("\n\n").

%% converts a letter number combination to a more intuitive X,Y array index
tuplefy(S) ->
    Y = (lists:nth(1,S)-97),
    X = 7-(lists:nth(2,S)-49),
    {X,Y}.

%% converts tuple to list index
index({X,Y}) ->
    X*8 + Y + 1.

%% returns the color of a piece at a tuple
getColor({X,Y},Board) ->
    (lists:nth(index({X,Y}),Board))#p.c.

%% returns the type of piece at a tuple
getPiece({X,Y},Board) ->
    (lists:nth(index({X,Y}),Board))#p.t.

%% returns the cell at a tuple
getCell({X,Y},Board) ->
    (lists:nth(index({X,Y}),Board)).

%% the test board
p_test() ->
    _Board = [#p{t=r,c=b}, #p{t=n,c=b}, #p{t=b,c=b}, #p{t=q,c=b}, #p{t=k,c=b}, #p{t=b,c=b}, #p{t=n,c=b}, #p{t=r,c=b}, 
	     #p{t=r,c=w}, #p{t=p,c=b}, #p{t=p,c=b}, #p{t=p,c=b}, #p{t=p,c=b}, #p{t=p,c=b}, #p{t=p,c=b}, #p{t=p,c=b},
	     #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e},
	     #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e},
	     #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e},
	     #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e},	   
	     #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e},	   
	     #p{t=r,c=w}, #p{t=n,c=w}, #p{t=b,c=w}, #p{t=q,c=w}, #p{t=k,c=w}, #p{t=b,c=w}, #p{t=n,c=w}, #p{t=b,c=w}
	    ].

%% the real chessboard
p() ->
    _Board = [#p{t=r,c=b}, #p{t=n,c=b}, #p{t=b,c=b}, #p{t=q,c=b}, #p{t=k,c=b}, #p{t=b,c=b}, #p{t=n,c=b}, #p{t=r,c=b}, 
	     #p{t=p,c=b}, #p{t=p,c=b}, #p{t=p,c=b}, #p{t=p,c=b}, #p{t=p,c=b}, #p{t=p,c=b}, #p{t=p,c=b}, #p{t=p,c=b},
	     #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e},
	     #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e},
	     #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e},
	     #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e}, #p{t=e,c=e},	   
	     #p{t=p,c=w}, #p{t=p,c=w}, #p{t=p,c=w}, #p{t=p,c=w}, #p{t=p,c=w}, #p{t=p,c=w}, #p{t=p,c=w}, #p{t=p,c=w},
	     #p{t=r,c=w}, #p{t=n,c=w}, #p{t=b,c=w}, #p{t=q,c=w}, #p{t=k,c=w}, #p{t=b,c=w}, #p{t=n,c=w}, #p{t=b,c=w}
	    ].

o() ->
    _Out = {[],[]}.


game(Board, Out) ->
    %% printBoard(Board),
    %% printOffBoard(Out),
    
    receive
	{From, {move, [Pos,Dir,N]}} ->
	    Coord = tuplefy(Pos),
	    case isValidDir(getCell(Coord,Board),Dir) of
		true ->
		    From ! {self(),ok},
		    Pair = targetPair(Coord,N,Dir,Board),
		    case equal(Pair) of
			true ->
			    Out2 = Out,
			    Board2 = Board;
			false ->
			    Out2 = popPiece(Pair,Board,Out),
			    Board2 = movePiece(Pair, Board)
		    end,
		    printBoard(Board2),
		    printOffBoard(Out2),
		    game(Board2,Out2);
		false ->
		    From ! {self(),ok},
		    printBoard(Board),
		    printOffBoard(Out),
		    game(Board,Out)
	    end;
	terminate -> ok
    end.

equal([P1,P2]) ->
    P1 == P2.
					   
move(Pid, [Pos,Dir,N]) ->
    Pid ! {self(), {move, [Pos,Dir,N]}},
	   receive
	       {Pid, Msg} -> Msg
	   end.

start() ->
    spawn(?MODULE, game, [p(),o()]).

main() ->
    printBoard(p()),
    io:format("\n\n"),
    Pid = start(),
    loop(Pid).

loop(Pid) ->    
    Line = io:get_line("Enter Move> "),
    Tokens = string:tokens(Line," \n"),
    Pos = lists:nth(1,Tokens),
    Dir = list_to_atom(string:to_lower(lists:nth(2,Tokens))),
    case length(Tokens) == 2 of
	true ->
	    N = 8;
	false ->
	    N = lists:nth(1,lists:nth(3,Tokens))-48
    end,
%    io:format(lists:concat(["Pos: ", Pos, "\nDir: ", Dir, "\nN: ", N, "~n"])),
    move(Pid,[Pos,Dir,N]),
    loop(Pid).
