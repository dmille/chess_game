#CPS506 Chess Assignment#
As part of my functional programming course, we were required to implement a chess game in various languages.

###Chess Assignment in Haskell###
haskell/chess.hs
Mark achieved: 9.5/10

###Chess Assignment in Clojure###
clojure/chess.clj
Mark achieved: 5/5

###Chess Assignment in Erlang###
erlang/chess.erl
Mark achieved: 5/5